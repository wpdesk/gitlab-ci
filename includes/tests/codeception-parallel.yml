prepare codeception parallel:
  tags:
    - codeception
    - $TESTS_TAG
  image:
    name: gitlab.wpdesk.dev:5050/wpdesk/docker-tests/codeception:13.1.0
    entrypoint: [""]
  only:
    variables:
      - $CODECEPTION_PARALLEL
  needs:
    - prepare prefixed vendor
    - prepare translations
    - job: prepare tests
      optional: true
    - job: prepare npm assets
      optional: true
  variables:
    WP_CLI_CACHE_DIR: /cache/wp-cli
    APACHE_DOCUMENT_ROOT: ${CI_PROJECT_DIR}/tests/wordpress
    SCREEN_WIDTH: 1200
    SCREEN_HEIGHT: 1200
  services:
    - name: mysql:5.7
      alias: mysqltests
      command: [--max-allowed-packet=67108864]
    - name: wpdesknet/wordpress:52
      alias: wootests
  artifacts:
    when: always
    paths:
      - tests/codeception/tests
      - ${APACHE_DOCUMENT_ROOT}
  stage: tests
  allow_failure: false
  interruptible: true
  before_script:
    - !reference [.composer-auth, script]
    - echo ${WPDESK_CI_VERSION}
    - php --version
  script:
    - echo "xdebug.max_nesting_level=2560" >> /usr/local/etc/php/php.ini
    - export DEPENDENT_PLUGINS_DIR=${CI_PROJECT_DIR}/tests/dependent_plugins
    - mkdir -p /cache/wp-cli
    - export MYSQL_IP=$(awk '/^[[:space:]]*($|#)/{next} /mysqltests/{print $1; exit}' /etc/hosts)
    - export WOOTESTS_IP=$(awk '/^[[:space:]]*($|#)/{next} /wootests/{print $1; exit}' /etc/hosts)
    - export TEST_SITE_WP_URL="http://${WOOTESTS_IP}"
    - cd ${CI_PROJECT_DIR}
    - mkdir -p /project
    - cd ${CI_PROJECT_DIR}
    - mkdir -p ${APACHE_DOCUMENT_ROOT}
    - cd ${APACHE_DOCUMENT_ROOT}
    - cd ${CI_PROJECT_DIR}
    - composer prepare-wordpress-for-codeception
    - composer prepare-local-codeception-tests
    - mkdir -p ${CI_PROJECT_DIR}/tests/codeception/tests/_output
    - mkdir -p ${CI_PROJECT_DIR}/tests/codeception/tests/acceptance
    - mkdir -p ${CI_PROJECT_DIR}/tests/codeception/tests/functional
    - mkdir -p ${CI_PROJECT_DIR}/tests/codeception/tests/unit
    - chmod -R a+w ${APACHE_DOCUMENT_ROOT}/wp-content/uploads
    - touch ${APACHE_DOCUMENT_ROOT}/wp-content/debug.log
    - chmod a+w ${APACHE_DOCUMENT_ROOT}/wp-content/debug.log
    - echo "End before"

.template: &job-codeception-test-parallel
  tags:
    - codeception
    - $TESTS_TAG
  image:
    name: gitlab.wpdesk.dev:5050/wpdesk/docker-tests/codeception:13.1.0
    entrypoint: [""]
  needs:
    - prepare prefixed vendor
    - prepare translations
    - prepare codeception parallel
    - job: prepare tests
      optional: true
    - job: prepare npm assets
      optional: true
  variables:
    WP_CLI_CACHE_DIR: /cache/wp-cli
    APACHE_DOCUMENT_ROOT: ${CI_PROJECT_DIR}/tests/wordpress
    SCREEN_WIDTH: 1200
    SCREEN_HEIGHT: 1200
  parallel: 4
  retry: 2
  services:
    - name: mysql:5.7
      alias: mysqltests
      command: [--max-allowed-packet=67108864]
    - name: wpdesknet/wordpress:52
      alias: wootests
    - name: selenium/standalone-chrome:latest
      alias: chrome
  artifacts:
    when: always
    paths:
      - tests/codeception/tests/_output
    reports:
      junit: tests/codeception/tests/_output/report.xml
  stage: tests
  allow_failure: false
  interruptible: true
  before_script:
    - export PARALLEL_JOB=${CI_NODE_INDEX}
  script:
    - echo "xdebug.max_nesting_level=2560" >> /usr/local/etc/php/php.ini
    - export DEPENDENT_PLUGINS_DIR=${CI_PROJECT_DIR}/tests/dependent_plugins
    - mkdir -p /cache/wp-cli
    - export MYSQL_IP=$(awk '/^[[:space:]]*($|#)/{next} /mysqltests/{print $1; exit}' /etc/hosts)
    - export WOOTESTS_IP=$(awk '/^[[:space:]]*($|#)/{next} /wootests/{print $1; exit}' /etc/hosts)
    - export TEST_SITE_WP_URL="http://${WOOTESTS_IP}"
    - cd ${APACHE_DOCUMENT_ROOT}
    - wp config set WP_SITEURL ${TEST_SITE_WP_URL} --allow-root
    - wp config set WP_HOME ${TEST_SITE_WP_URL} --allow-root
    - wp config set DB_HOST ${MYSQL_IP} --allow-root
    - cd ${CI_PROJECT_DIR}
    - composer prepare-parallel-codeception-tests ${CI_NODE_TOTAL}
    - vendor/bin/codecept run acceptance $PARALLEL_JOB --steps --xml --html -f --verbose
  after_script:
    - !reference [.after-codeception, after_script]
    - 'if [[ ${REPORTPORTAL_AUTHORIZATION} && ${REPORTPORTAL_PROJECT} ]]; then curl -X POST "http://195.201.225.204:8080/api/v1/${REPORTPORTAL_PROJECT}/launch/import" -H "accept: */*" -H "Content-Type: multipart/form-data" -H "${REPORTPORTAL_AUTHORIZATION}" -F "file=@tests/codeception/tests/_output/report.xml;type=text/xml;filename=${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}-codeception.xml"; fi'

.template: &job-codeception-test-parallel-auto
  <<: *job-codeception-test-parallel
  only:
    refs:
      - master
      - main
      - devel
      - tags
    variables:
      - $CODECEPTION_PARALLEL

.template: &job-codeception-test-parallel-manual
  <<: *job-codeception-test-parallel
  when: manual
  allow_failure: true
  only:
    variables:
      - $CODECEPTION_PARALLEL

.template: &job-codeception-test-parallel-manual-recorded
  <<: *job-codeception-test-parallel-manual
  when: manual
  allow_failure: true
  before_script:
    - cp vendor/wpdesk/wp-codeception/configuration/codeception-with-recorder.dist.yml codeception-with-recorder.dist.yml
  after_script:
    - !reference [.after-codeception, after_script]
    - 'if [[ ${REPORTPORTAL_AUTHORIZATION} && ${REPORTPORTAL_PROJECT} ]]; then curl -X POST "http://195.201.225.204:8080/api/v1/${REPORTPORTAL_PROJECT}/launch/import" -H "accept: */*" -H "Content-Type: multipart/form-data" -H "${REPORTPORTAL_AUTHORIZATION}" -F "file=@tests/codeception/tests/_output/report.xml;type=text/xml;filename=${CI_PROJECT_NAME}-${CI_COMMIT_REF_SLUG}-codeception.xml"; fi'
    - 'curl -X POST --data-urlencode "payload={\"text\": \"Testy projektu <${CI_PROJECT_URL}|${CI_PROJECT_NAME}> zostały wykonane. <${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}/artifacts/browse/tests/codeception/tests/_output/records.html|Zapis testu>\", }" https://hooks.slack.com/services/${SLACK_AUTH}'

.template: &job-codeception-coverage
  <<: *job-codeception-test-parallel-manual
  services:
    - name: mysql:5.7
      alias: mysqltests
    - name: wpdesknet/wordpress:52
      alias: wootests
    - name: selenium/standalone-chrome:latest
      alias: chrome
  script:
    - echo "xdebug.max_nesting_level=2560" >> /usr/local/etc/php/php.ini
    - export DEPENDENT_PLUGINS_DIR=${CI_PROJECT_DIR}/tests/dependent_plugins
    - mkdir -p /cache/wp-cli
    - export MYSQL_IP=$(awk '/^[[:space:]]*($|#)/{next} /mysqltests/{print $1; exit}' /etc/hosts)
    - export WOOTESTS_IP=$(awk '/^[[:space:]]*($|#)/{next} /wootests/{print $1; exit}' /etc/hosts)
    - export TEST_SITE_WP_URL="http://${WOOTESTS_IP}"
    - cd ${CI_PROJECT_DIR}
    - mkdir -p /project
    - cd ${CI_PROJECT_DIR}
    - mkdir -p ${APACHE_DOCUMENT_ROOT}
    - cd ${APACHE_DOCUMENT_ROOT}
    - cd ${CI_PROJECT_DIR}
    - composer prepare-wordpress-for-codeception
    - composer prepare-local-codeception-tests-with-coverage
    - mkdir -p ${CI_PROJECT_DIR}/tests/codeception/tests/_output
    - mkdir -p ${CI_PROJECT_DIR}/tests/codeception/tests/acceptance
    - mkdir -p ${CI_PROJECT_DIR}/tests/codeception/tests/functional
    - mkdir -p ${CI_PROJECT_DIR}/tests/codeception/tests/unit
    - chmod -R a+w ${APACHE_DOCUMENT_ROOT}/wp-content/uploads
    - touch ${APACHE_DOCUMENT_ROOT}/wp-content/debug.log
    - chmod a+w ${APACHE_DOCUMENT_ROOT}/wp-content/debug.log
    - echo "End before"
    - cd ${CI_PROJECT_DIR}
    - vendor/bin/codecept run acceptance --steps --xml --html -f --coverage --coverage-xml --coverage-html --verbose

codeception parallel test:
  <<: *job-codeception-test-parallel-auto

codeception paralel test manual:
  <<: *job-codeception-test-parallel-manual

codeception parallel test manual recorded:
  <<: *job-codeception-test-parallel-manual-recorded
  before_script:
    - export PARALLEL_JOB=$CI_NODE_INDEX
    - cp vendor/wpdesk/wp-codeception/configuration/codeception-with-recorder.dist.yml codeception-with-recorder.dist.yml

codeception code coverage manual:
  <<: *job-codeception-coverage
  when: manual
  allow_failure: true
