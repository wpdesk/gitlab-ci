create table wpdesk_plugin_deploys (
    deploy_id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    deploy_time timestamp NOT NULL,
    project_name varchar(50) NOT NULL,
    plugin_version varchar(20) NOT NULL,
    wp_tested_up varchar(20) NOT NULL,
    wc_tested_up varchar(20) NOT NULL,
    INDEX(project_name),
    INDEX(project_name, deploy_time)
);

alter table wpdesk_plugin_deploys add (
    wp_requires_at_least varchar(20) NOT NULL,
    wc_requires_at_least varchar(20) NOT NULL,
    php_requires varchar(20) NOT NULL
);

create or replace view wpdesk_plugin_deploys_current as
    select project_name, plugin_version, wp_tested_up, wc_tested_up, deploy_time, wp_requires_at_least, wc_requires_at_least, php_requires
    from wpdesk_plugin_deploys as d1
    where deploy_id = (select max(deploy_id) from wpdesk_plugin_deploys as d2 where d1.project_name = d2.project_name);
